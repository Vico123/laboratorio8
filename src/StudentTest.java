import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StudentTest {

    @Test
    public void testMain(){

        Student student = new Student(1, "Gery", List.of(new Subject("Ciencia", 67.0), new Subject("Matematicas", 70.0), new Subject("Ingles", 69.0)));
        List<String> subjectsStudent = List.of("Ciencia", "Matematicas", "Ingles");

        List<String> subjectsObtained = new ArrayList();
        for (Subject subject : student.getSubjects()){
            subjectsObtained.add(subject.getSubject());
        }
        double averageObtained = student.calculateAverage();
        assertEquals(68.66666666666667, averageObtained, 0.01);
        assertEquals(subjectsStudent, subjectsObtained);

    }
}
