import java.util.List;

public class Student {
    private int id;
    private String name;
    private List<Subject> subjects;

    public Student(int id, String name, List<Subject> subjects){

        this.id = id;
        this.name = name;
        this.subjects = subjects;

    }

    public double calculateAverage(){

        double suma = 0;
        for(Subject subject : subjects){

            suma += subject.getScore();

        }

        return suma / subjects.size();

    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void printInfoStudent(){

        System.out.println("Estudiante " + id + ": " + name);
        System.out.println();
        for (Subject subject : subjects){

            System.out.println("Nota de " + subject.getSubject() + ": " + subject.getScore());

        }

        System.out.println("Nota Promedio: " + calculateAverage());
        System.out.println();
    }

}
