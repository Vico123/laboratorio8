public class Subject {

    private String subject;
    private double score;

    public Subject(String subject, double score){

        this.subject = subject;
        this.score = score;

    }

    public String getSubject() {
        return subject;
    }

    public double getScore() {
        return score;
    }
}
