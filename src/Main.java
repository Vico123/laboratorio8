import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Student> students = new ArrayList();

        students.add(new Student(1, "Gery", List.of(new Subject("Ciencia", 67.0), new Subject("Matematicas", 70.0), new Subject("Ingles", 69.0))));
        students.add(new Student(2, "Joel", List.of(new Subject("Ciencia", 80.0), new Subject("Matematicas", 85.0), new Subject("Ingles", 85.0))));
        students.add(new Student(3, "Carmen", List.of(new Subject("Ciencia", 90.5), new Subject("Matematicas", 65.5), new Subject("Ingles", 95.5))));
        students.add(new Student(4, "Lia", List.of(new Subject("Ciencia", 75.3), new Subject("Matematicas", 75.3), new Subject("Ingles", 85.3), new Subject("Literatura", 70.0))));
        students.add(new Student(5, "Phill", List.of(new Subject("Ciencia", 70.0), new Subject("Matematicas", 80.0), new Subject("Ingles", 74.0))));

        for (Student student : students){
            student.printInfoStudent();
        }

    }

}