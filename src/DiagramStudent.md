classDiagram
direction BT
class Main {
  + main(String[]) void
}
class Student {
  - String name
  - int id
  - List~Subject~ subjects
  + calculateAverage() double
  + getSubjects() List~Subject~
  + printInfoStudent() void
}
class StudentTest {
  + testMain() void
}
class Subject {
  - double score
  - String subject
  + getSubject() String
  + getScore() double
}

Main  ..>  Student : «create»
Main  ..>  Subject : «create»
Student "1" *--> "subjects *" Subject 
StudentTest  ..>  Student : «create»
StudentTest  ..>  Subject : «create»
